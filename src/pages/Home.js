import React, {Component} from "react";
import logo from "../logo.svg";
import { BiSearchAlt2 } from "react-icons/bi";
import "../App.css";
import "../pages/home.scss";
import { BrowserRouter as Router, Route } from "react-router-dom";
import Header from "../Components/Header/Header";
import '../App.css';




class Home extends React.Component{

  state ={
    email: '',
    username: ''
  }
  componentDidMount() {
    fetch("http://localhost:8003/user/", {
      method: 'GET',
      headers:{
        "access-control-allow-origin":"*",
      }
      
    }
   


    
    )
      .then(res => res.json())
      .then(
        (result) => {
          console.log(result);
          /* this.setState({
            isLoaded: true,
            items: result.items
          }); */
        },
        // Nota: es importante manejar errores aquí y no en 
        // un bloque catch() para que no interceptemos errores
        // de errores reales en los componentes.
        (error) => {
         /*  this.setState({
            isLoaded: true,
            error
          }); */
        }
      )
  }

  render(){
   

    
 
  return (
    <div className="App">
      <Header></Header>
      <div className="container">
        <div className="row">
          <h1>Clientes</h1>
        </div>
        <div className="row">
          <div className="col-md-4">
            <span>
              <BiSearchAlt2 className="svg-search" />
            </span>{" "}
            <input className="search-users" type="text" />
            <table class="table table-striped tabla-clientes table-hover">
              <thead class="thead-light">
                <tr>
                  <th scope="col">Nro Identificacion</th>
                  <th scope="col">Nombre</th>
                 
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th scope="row">1140888254</th>
                  <td>Mark Alexandro de Alva</td>
                  
                </tr>
                <tr>
                  <th scope="row">1045707034</th>
                  <td>Jacob Joel Castillo Perez</td>
               
                </tr>
                <tr>
                  <th scope="row">123365874</th>
                  <td>Larry Came Melo Lopez</td>
                
                </tr>
              </tbody>
            </table>

            <button className="btn btn-primary btn-agregar">Agregar</button>
          </div>
          <div className="col-md-8"></div>
        </div>
      </div>
    </div>
  );
}

}

export default Home;
