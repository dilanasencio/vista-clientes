import { Component } from "react";
import { BsJustify } from "react-icons/bs";
import logoFocaHorizontal from "../../assets/img/foca-logo-horizontal.png";
import userLogo from "../../assets/img/user-img.png";
import './header-style.scss';
class Header extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            username:""
         }
    }
    render() { 
        return ( 
            <div className="header-container">
                <img className="foca-logo-header" src={logoFocaHorizontal}/>
                <div className="right-side-icons">
                    <BsJustify className="icon-menu-header hoverPointer"/>
                    <img className="user-img-header hoverPointer" src={userLogo}/>
                </div>
            </div>
         );
    }
}
 
export default Header;